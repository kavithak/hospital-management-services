package com.management.services.hospitalmanagementservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalManagementServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(HospitalManagementServicesApplication.class, args);
	}

}
